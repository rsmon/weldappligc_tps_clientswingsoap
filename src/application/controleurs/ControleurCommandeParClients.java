package application.controleurs;

import application.modele.Modele;
import application.modele.ModeleCommandesParClients;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.LinkedList;
import java.util.List;
import webservice.soap.client.Client;
import webservice.soap.client.ResumeCommande;


public class ControleurCommandeParClients {
    
    private ModeleCommandesParClients modele;
    
    private List<Client>              lesClients          = new LinkedList<Client>();
    private Client                    clientSel           = null;
    private List<ResumeCommande>      lesResumesCommandes = new LinkedList<ResumeCommande>();
    private Float                     caAnnuelClient      = 0F;
    private Float                     soldeClient         = 0F; 
   
    public void init(Modele unModele){
       
       this.modele=(ModeleCommandesParClients)unModele;
       this.setLesClients(modele.getTousLesClients());  
      
    }  
    
    public void traiterSelectionCombo(){
    
      if(clientSel!=null){
          
           this.setLesResumesCommandes(modele.getResumesCommandes(clientSel));
           System.out.println(lesResumesCommandes);
           this.setCaAnnuelClient(modele.caAnneeEnCours(clientSel));
           this.setSoldeClient(modele.getSoldeClient(clientSel));
      } 
    } 
    
    //<editor-fold defaultstate="collapsed" desc="CODE GENERE">
    
    public static final String PROP_LESCLIENTS = "lesClients";
    
    public List<Client> getLesClients() {
        return lesClients;
    }
    
    public void setLesClients(List<Client> lesClients) {
        List<Client> oldLesClients = this.lesClients;
        this.lesClients = lesClients;
        propertyChangeSupport.firePropertyChange(PROP_LESCLIENTS, oldLesClients, lesClients);
    }
    
    
    public static final String PROP_CLIENTSEL = "clientSel";
    
    public Client getClientSel() {
        return clientSel;
    }
    
    public void setClientSel(Client clientSel) {
        Client oldClientSel = this.clientSel;
        this.clientSel = clientSel;
        propertyChangeSupport.firePropertyChange(PROP_CLIENTSEL, oldClientSel, clientSel);
    }
    
    public static final String PROP_LESRESUMESCOMMANDES = "lesResumesCommandes";
    
    public List<ResumeCommande> getLesResumesCommandes() {
        return lesResumesCommandes;
    }
    
    public void setLesResumesCommandes(List<ResumeCommande> lesResumesCommandes) {
        List<ResumeCommande> oldLesResumesCommandes = this.lesResumesCommandes;
        this.lesResumesCommandes = lesResumesCommandes;
        propertyChangeSupport.firePropertyChange(PROP_LESRESUMESCOMMANDES, oldLesResumesCommandes, lesResumesCommandes);
    }
    
    
    public static final String PROP_CAANNUELCLIENT = "caAnnuelClient";
    
    public Float getCaAnnuelClient() {
        return caAnnuelClient;
    }
    
    public void setCaAnnuelClient(Float caAnnuelClient) {
        Float oldCaAnnuelClient = this.caAnnuelClient;
        this.caAnnuelClient = caAnnuelClient;
        propertyChangeSupport.firePropertyChange(PROP_CAANNUELCLIENT, oldCaAnnuelClient, caAnnuelClient);
    }
    
    
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    public static final String PROP_SOLDECLIENT = "soldeClient";

    public Float getSoldeClient() {
        return soldeClient;
    }

    public void setSoldeClient(Float soldeClient) {
        Float oldSoldeClient = this.soldeClient;
        this.soldeClient = soldeClient;
        propertyChangeSupport.firePropertyChange(PROP_SOLDECLIENT, oldSoldeClient, soldeClient);
    }
  
    //</editor-fold>

}



   