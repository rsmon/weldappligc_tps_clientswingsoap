/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package application.rendus;

/**
 *
 * @author rsmon
 */

import java.awt.Color;
import javax.swing.table.DefaultTableCellRenderer;

  public class RenduFeuTricolore extends DefaultTableCellRenderer {

    public RenduFeuTricolore() {
      super();
      setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
     
    }

    @Override
    public void setValue(Object value) {
      if((Float)value>1) setBackground(Color.RED);
      else if((Float)value>0.75f)setBackground(Color.ORANGE);
      else setBackground(Color.GREEN);
      super.setValue(null);
    }

  }

