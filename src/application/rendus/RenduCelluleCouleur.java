/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package application.rendus;

/**
 *
 * @author rsmon
 */

import java.awt.Color;
import javax.swing.table.DefaultTableCellRenderer;

  public class RenduCelluleCouleur extends DefaultTableCellRenderer {

    public RenduCelluleCouleur() {
      super();
      setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
     
    }

    @Override
    public void setValue(Object value) {
      String s=(String)value;  
      if(s.equals("L")) this.setBackground(Color.RED);
      else setBackground(Color.GREEN);
      super.setValue(s);
    }

  }

