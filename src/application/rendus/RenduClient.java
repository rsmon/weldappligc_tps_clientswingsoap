package application.rendus;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import webservice.soap.client.Client;


/**
 *
 * @author rsmon
 */

public class RenduClient extends JLabel implements ListCellRenderer{

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Client c = (Client)value;
        JLabel renderer = new JLabel(c.getNomCli());
        if (isSelected) {
            renderer.setOpaque(true);
            renderer.setForeground(Color.BLUE);
            renderer.setBackground(Color.GRAY);
        }
        return renderer;
    }
    
}
    


