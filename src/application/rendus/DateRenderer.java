package application.rendus;

import java.util.Date;
import javax.swing.table.DefaultTableCellRenderer;
import javax.xml.datatype.XMLGregorianCalendar;
import utilitaires.UtilDate;


  public class DateRenderer extends DefaultTableCellRenderer {

    public DateRenderer() {
      super();
      setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
    }

    @Override
    public void setValue(Object value) {
      if ((value != null) && (value instanceof Date)) {
       Date date=(Date)value; 
       value = UtilDate.format(date);
      }
      if ((value != null) && (value instanceof XMLGregorianCalendar)) {
       
           Date date=((XMLGregorianCalendar)value).toGregorianCalendar().getTime();
           value = UtilDate.format(date);    
      }
      
      super.setValue(value);
    }

  }

