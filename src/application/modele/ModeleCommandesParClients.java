package application.modele;
import java.util.List;
import webservice.soap.client.Client;
import webservice.soap.client.ResumeCommande;


public interface ModeleCommandesParClients extends Modele{
 
    List<Client>          getTousLesClients();   
    Float                 caAnneeEnCours(Client pClient);
    List<ResumeCommande>  getResumesCommandes(Client pClient);
    Float                 getSoldeClient(Client pClient);  
}


