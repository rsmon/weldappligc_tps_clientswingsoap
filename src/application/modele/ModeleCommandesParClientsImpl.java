
package application.modele;

import java.io.Serializable;
import java.util.List;
import javax.inject.Singleton;
import webservice.soap.client.Client;
import webservice.soap.client.ResumeCommande;
import webservice.soap.client.WebServClient;
import webservice.soap.client.WebServClient_Service;


@Singleton
public class ModeleCommandesParClientsImpl implements ModeleCommandesParClients, Serializable {

    private WebServClient_Service service ;
    private WebServClient         ws;

    public ModeleCommandesParClientsImpl() {
        
        this.service = new WebServClient_Service(); 
     
        this.ws      = service.getWebServClientPort();
            
    }
 
    @Override
    public List<Client> getTousLesClients() {
        return ws.getTouslesClients();
    }
   
    @Override
    public Float caAnneeEnCours(Client client) {
        return ws.caAnneeEncoursClient(client.getNumCli());
    }

    @Override
    public List<ResumeCommande> getResumesCommandes(Client pClient) {
       
       return ws.getLesResumesCommandeClient(pClient.getNumCli());
    }

    
    @Override
    public Float getSoldeClient(Client pClient) {
        return ws.getSoldeClient(pClient.getNumCli()); 
    }
     
}



