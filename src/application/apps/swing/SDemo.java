package application.apps.swing;
import application.boot.AppliWeld;
import application.modele.ModeleCommandesParClients;
import application.vues.VueCommandesParClients;
import javax.inject.Inject;

public class SDemo implements AppliWeld{
       
    @Inject ModeleCommandesParClients modele;
      
    @Override
    public void run(){
         
          new VueCommandesParClients(modele).setVisible(true);
          
    }
}


